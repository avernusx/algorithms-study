defmodule FactorialModule do
  def factorial(n) do
    if n == 1 do
      1
    else
      n * FactorialModule.factorial(n - 1)
    end
  end
end

IO.puts(FactorialModule.factorial(10000))