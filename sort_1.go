package main

import "fmt"

func main() {
	array := [...]int{100, 7, 3, 8, 5}

	for i := 0; i < len(array) - 1; i++ {
		for j := i; j < len(array) - 1; j++ {
			if array[j] < array[i] {
				array[i], array[j] = array[j], array[i]
			}
		}
	}

	for i := 0; i < len(array) - 1; i++ { 
		fmt.Println(array[i])
	}
}